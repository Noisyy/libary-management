import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Account } from '../_module/account';
import { Member } from '../_module/member';
import { AcountService } from '../_services/acount.service';
import { MemberService } from '../_services/member.service';



@Component({
  selector: 'app-login"',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // @Output() foo1:EventEmitter<any> = new EventEmitter(); 
  message : string ="Hello from child";


  loginForm : FormGroup;
  registerForm : FormGroup;
  tempAccount : Account = new Account();
  member : Member = new Member();
  isLogin :boolean = true;
  isRegister : boolean ;
  constructor( 
      private accountService : AcountService, 
      private router : Router,
      private route : ActivatedRoute,
      private memberService : MemberService
      ){}
  ngOnInit()
  {
   this.loginForm=new FormGroup
   ({
      username : new FormControl(null),
      password : new FormControl(null)
   });
   
  this.registerForm = new FormGroup
  ({
      name : new FormControl(null),
      phone : new FormControl(null),
      address : new FormControl(null),
      username : new FormControl(null),
      password : new FormControl(null)
  });
  
 
  }
  

  
  onSubmit()
  {
    // console.log(this.loginForm);
    // console.log(this.loginForm.value.password);
    this.tempAccount.username = this.loginForm.value.username;
    this.tempAccount.password = this.loginForm.value.password;
    
    this.accountService.checkLogin(this.tempAccount).subscribe(
      data =>{
        if(data!==null)
        {
          this.tempAccount.id=data['id'];
          this.tempAccount.role=data['role'];
          if(this.tempAccount.role===1)
          {
            
            this.router.navigate(['admin']);
          }
         else
         {
          
          this.router.navigate(['home']);
         }
     
         sessionStorage.setItem('account_id',(this.tempAccount.id).toString());
        }
        
      },error => console.log("Error")
      
    )

  }
 

  onRegister()
  {
    this.member.name = this.registerForm.value.name;
    this.member.phone = this.registerForm.value.phone;
    this.member.address = this.registerForm.value.address;
    let a= new Account();
    a.username  = this.registerForm.value.username;
    a.password = this.registerForm.value.password;
    this.member.account =a;
    console.log(this.registerForm);
    this.memberService.createMember(this.member).subscribe(data=>{
      if(data!==null)
      {
        alert("Create successfully!");
        this.registerForm.reset();
        this.router.navigate(['login']);
      }
    }
    )
  }
  onChange()
  {
    if(this.isLogin)
    {
      this.isLogin=false;
      this.isRegister=!this.isLogin;
    }
    else
    {
      this.isLogin=true;
      this.isRegister=!this.isLogin;
    }
  }
}
