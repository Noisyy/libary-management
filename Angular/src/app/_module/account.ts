export class Account {
    id:number=null;
    username : string;
    password : string;
    role : number=2;
    status : string='active';
}
