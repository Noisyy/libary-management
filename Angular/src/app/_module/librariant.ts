import { Account } from "./account";

export class Librariant {
    id :number;
    name: string;
    phone:string;
    account : Account;
}
