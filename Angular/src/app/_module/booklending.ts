
import { Bookitem } from "./bookitem";

export class Booklending {
    id : number = null;
    startD : string;
    endDate : string;
    status : string="not accept";
    list : Array<Bookitem>=[];
}
