export class Bookitem {
    id : number;
    title : string;
    category: string;
    author : string;
    publisher : string;
    image : string;
    numberOfPage : number;
    amount : number;
    position : string;
    price : number;
}
