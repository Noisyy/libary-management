import { Account } from "./account";

export class Member {
    id : number;
    name : string;
    phone : string;
    address : string;
    account : Account ;
}
