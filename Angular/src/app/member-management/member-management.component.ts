import { Component, OnInit } from '@angular/core';
import { Member } from '../_module/member';
import { BooklendingService } from '../_services/booklending.service';
import { MemberService } from '../_services/member.service';

@Component({
  selector: 'app-member-management',
  templateUrl: './member-management.component.html',
  styleUrls: ['./member-management.component.css']
})
export class MemberManagementComponent implements OnInit{

  listMember : Array<Member>=[];
  constructor(
    private memberService : MemberService,
    private booklendingService : BooklendingService
  ){}
  ngOnInit(): void {
    this.getData();
  }
  getData()
  {
    this.memberService.getAll()
    .subscribe(data=>{
      this.listMember=data;
    })
  }
  acceptBookLending(member_id :number):boolean{
      this.booklendingService.acceptBookLendingForMember(member_id)
      .subscribe(data=>
        {
          console.log(data);
          return data;
        });
        return false;
  }
  
}
