import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Bookitem } from '../_module/bookitem';
import { Booklending } from '../_module/booklending';
import { BookService } from '../_services/book.service';
import { BooklendingService } from '../_services/booklending.service';

@Component({
  selector: 'app-list-book-item-added',
  templateUrl: './list-book-item-added.component.html',
  styleUrls: ['./list-book-item-added.component.css']
})
export class ListBookItemAddedComponent implements OnInit {
  

  tempBookLending : Booklending = new Booklending();
  listBookitem : Array<Bookitem>=[];
  member_id : number;
  constructor(
    private bookitemService : BookService,
    private router : Router,
    private booklendingService : BooklendingService
    ){}
  
  ngOnInit(): void {
    this.getDataFromLocalStorage();
  }

  getDataFromLocalStorage()
  {
    if(localStorage.getItem('tempBookLending')!==null)
    {
      let temp = localStorage.getItem('tempBookLending');
        this.tempBookLending =JSON.parse(temp);
        this.listBookitem = this.tempBookLending.list;
        
    }

    if(sessionStorage.getItem('member_id')!==null)
    {
      this.member_id= parseInt(sessionStorage.getItem('member_id'));
      // console.log(this.member_id);
    }
    
  }
  removeBook(id :number){
    console.log(id);
    this.tempBookLending.list =  this.tempBookLending.list.filter(bookitem => bookitem.id!==id);
    localStorage.setItem('tempBookLending',JSON.stringify(this.tempBookLending));
    // this.router.navigate(['list-book-added']);
    window.location.reload();
    console.log(this.tempBookLending.list);

  }
  onSubmit()
  {
    console.log("Submit");
    this.booklendingService.submitBooklending(this.member_id,this.tempBookLending)
    .subscribe(data=>{
      console.log(data);
    })

    localStorage.removeItem('tempBookLending');
    window.location.reload();
  }
}
