import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBookItemAddedComponent } from './list-book-item-added.component';

describe('ListBookItemAddedComponent', () => {
  let component: ListBookItemAddedComponent;
  let fixture: ComponentFixture<ListBookItemAddedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListBookItemAddedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListBookItemAddedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
