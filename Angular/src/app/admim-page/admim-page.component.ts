import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Account } from '../_module/account';
import { Librariant } from '../_module/librariant';
import { LibrariantService } from '../_services/librariant.service';

@Component({
  selector: 'app-admim-page',
  templateUrl: './admim-page.component.html',
  styleUrls: ['./admim-page.component.css']
})
export class AdmimPageComponent implements OnInit{
  account_id : number;
  librariant : Librariant = new Librariant();
  constructor(
    private router : Router,
    private libraService : LibrariantService
  ){}

  ngOnInit(): void {
   this.getData();
  }
  getData()
  {
    if(sessionStorage.getItem('account_id')!==null)
    {
      this.account_id = parseInt(sessionStorage.getItem('account_id'));
      this.libraService.getByAccountId(this.account_id)
      .subscribe(data=>{
        this.librariant  = data;
      })
    }
  }

  logOut(){}
}
