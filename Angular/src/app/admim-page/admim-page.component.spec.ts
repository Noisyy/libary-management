import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmimPageComponent } from './admim-page.component';

describe('AdmimPageComponent', () => {
  let component: AdmimPageComponent;
  let fixture: ComponentFixture<AdmimPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmimPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdmimPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
