import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBooklendingComponent } from './list-booklending.component';

describe('ListBooklendingComponent', () => {
  let component: ListBooklendingComponent;
  let fixture: ComponentFixture<ListBooklendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListBooklendingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListBooklendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
