import { Component } from '@angular/core';
import { Account } from '../_module/account';
import { ActivatedRoute, Router } from '@angular/router';

import { Booklending } from '../_module/booklending';
import { Member } from '../_module/member';
import { MemberService } from '../_services/member.service';
import { BooklendingService } from '../_services/booklending.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-list-booklending',
  templateUrl: './list-booklending.component.html',
  styleUrls: ['./list-booklending.component.css']
})
export class ListBooklendingComponent {
  account : Account = new Account();
  member : Member = new Member();
  booklending : Booklending[] ;
  account_id :number;
  member_id :number;
  constructor(
    private route : ActivatedRoute, 
    private memberService : MemberService, 
    private router : Router, 
    private booklendingService : BooklendingService

    ){}

  ngOnInit(): void {
    
    this.account_id = parseInt(sessionStorage.getItem('account_id'));

    this.memberService.getMemberByAccountId(this.account_id).subscribe(data=>
      {
        if(data!==null)
        {

          console.log('get data');
          this.member.id = data['id'];
          this.member.name= data['name'];
          this.member.phone = data['phone'];
          this.member.address = data['address'];
          this.member_id = this.member.id;
          console.log(typeof data['id'] );
          console.log(data);
          
          this.booklendingService.getBookLendingByMemberId(this.member_id).subscribe
          (data1=>{
          this.booklending =data1;
        console.log(JSON.stringify(this.booklending));
      })
        }
      })
      // console.log(typeof data['id'] );
      
      
  }
}
