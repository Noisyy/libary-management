
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Account } from '../_module/account';
import { Booklending } from '../_module/booklending';
import { Member } from '../_module/member';
import { MemberService } from '../_services/member.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit{

  account : Account = new Account();
  member : Member = new Member();
  booklending : Booklending = new Booklending();
  constructor(
    private route : ActivatedRoute, 
    private memberService : MemberService, 
    private router : Router
    ){}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params :any)=>{
      this.account.id = params.data;
      console.log("User Component :"+this.account.id);
    })

    this.memberService.getMemberByAccountId(this.account.id).subscribe(data=>
      {
        if(data!==null)
        {
          
          this.member.id = data['id'];
          this.member.name= data['name'];
          this.member.phone = data['phone'];
          this.member.address = data['address'];
          this.booklending = data['listBookLending'];
          console.log('got data' +this.booklending);
        }
      })

  }

}
