import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Librariant } from '../_module/librariant';

@Injectable({
  providedIn: 'root'
})
export class LibrariantService {

  private baseUrl ='http://localhost:8003/librariant';
  constructor(private http : HttpClient) { }

  getByAccountId(id:number):Observable<Librariant>
  {

    return this.http.get<any>(`${this.baseUrl}/${id}`);

  }
}
