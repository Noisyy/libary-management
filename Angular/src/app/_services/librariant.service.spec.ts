import { TestBed } from '@angular/core/testing';

import { LibrariantService } from './librariant.service';

describe('LibrariantService', () => {
  let service: LibrariantService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LibrariantService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
