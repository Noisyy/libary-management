import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from '../_module/account';

@Injectable({
  providedIn: 'root'
})
export class AcountService {

  private baseUrl ='http://localhost:8003/account';
  constructor(private httpClient: HttpClient) { }
  checkLogin(account : Account) : Observable<any>
  {
    return this.httpClient.post(`${this.baseUrl}/login`,account);
  }

  blockOrUnBlockAccount(id : number, status : string ):Observable<any>
  {
    return this.httpClient.put(`${this.baseUrl}/blockOrUnblock/${id}`,status);
  }
  
}
