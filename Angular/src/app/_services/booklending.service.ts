import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Booklending } from '../_module/booklending';

@Injectable({
  providedIn: 'root'
})
export class BooklendingService {
  private baseUrl ='http://localhost:8003/booklending';
  constructor(private http : HttpClient) { }
  getBookLendingByMemberId(id : any ) :Observable<Booklending[]>
  {
    return this.http.get<Booklending[]>(`${this.baseUrl}/listByMemberId/${id}`);
  }
  submitBooklending( id : number,bookLending : Booklending) : Observable<Booklending>
  {
    return this.http.post<any>(`${this.baseUrl}/create/${id}`,bookLending);
  }

  acceptBookLendingForMember(member_id : number):Observable<any>
  {
    let status="accepted";
    return this.http.put(`${this.baseUrl}/accept/${member_id}`,status);
  }
}
