import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bookitem } from '../_module/bookitem';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private baseUrl ='http://localhost:8003/book';
  constructor(private httpClient : HttpClient) { }
  getListBookItemByTitle(title : string): Observable<Bookitem[]>
  {
    return this.httpClient.get<Bookitem[]>(`${this.baseUrl}/searchByTitle/${title}`);
  }
  getBookItemById(id: number): Observable<Bookitem>
  {
    return this.httpClient.get<any>(`${this.baseUrl}/${id}`);
  }
  getListBookItemByAuthor(author : string): Observable<Bookitem[]>
  {
    return this.httpClient.get<Bookitem[]>(`${this.baseUrl}/searchByAuthor/${author}`);
  }
  getListBookItemByPublisher(publisher : string): Observable<Bookitem[]>
  {
    return this.httpClient.get<Bookitem[]>(`${this.baseUrl}/searchByPublisher/${publisher}`);
  }
  updateBook(id : number, book : Bookitem):Observable<any>
  {
    return this.httpClient.put(`${this.baseUrl}/update/${id}`,book);
  }
  deleteBook(id : number):Observable<any>
  {
    return this.httpClient.delete(`${this.baseUrl}/delete/${id}`);
  }

  getListBookInBookLending(bookLending_id : number ):Observable<Bookitem[]>
  {
    return this.httpClient.get<any>(`${this.baseUrl}/findByBookLendingId/${bookLending_id}`);
  }

  getAllBook():Observable<Bookitem[]>
  {
    return this.httpClient.get<any>(`${this.baseUrl}/list`);
  }
  createNewBook(bookItem : Bookitem):Observable<any>
  {
    return this.httpClient.post<any>(`${this.baseUrl}/create`,bookItem);
  }
}
