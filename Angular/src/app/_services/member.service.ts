import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from '../_module/member';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  private baseUrl ='http://localhost:8003/member'

  constructor(private http : HttpClient) { }
  
  getMemberByAccountId(id : number):Observable<Member>{
    return this.http.get<Member>(`${this.baseUrl}/findByAccountId/${id}`);
  }

  createMember(member : Member):Observable<any>
  {
    return this.http.post(`${this.baseUrl}/create`,member);
  }
  getAll():Observable<Member[]>
  {
    return this.http.get<Array<Member>>(`${this.baseUrl}/list`);
  }
}
