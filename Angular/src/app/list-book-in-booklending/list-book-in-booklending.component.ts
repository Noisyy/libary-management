import { Component, OnInit } from '@angular/core';
import { Bookitem } from '../_module/bookitem';
import { BookService } from '../_services/book.service';

@Component({
  selector: 'app-list-book-in-booklending',
  templateUrl: './list-book-in-booklending.component.html',
  styleUrls: ['./list-book-in-booklending.component.css']
})
export class ListBookInBooklendingComponent implements OnInit{
  
  listBookitem : Array<Bookitem>=[];
  constructor(
    private bookService : BookService
  ){}
  ngOnInit(): void {
    this.bookService.getListBookInBookLending(1)
    .subscribe(data=>{
      this.listBookitem =data;
    })
  }
}
