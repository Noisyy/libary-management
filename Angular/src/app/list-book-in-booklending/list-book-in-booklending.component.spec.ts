import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBookInBooklendingComponent } from './list-book-in-booklending.component';

describe('ListBookInBooklendingComponent', () => {
  let component: ListBookInBooklendingComponent;
  let fixture: ComponentFixture<ListBookInBooklendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListBookInBooklendingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListBookInBooklendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
