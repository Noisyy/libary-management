import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Bookitem } from '../_module/bookitem';
import { BookService } from '../_services/book.service';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.css']
})
export class CreateBookComponent {
  bookItem : Bookitem = new Bookitem();
  createForm : FormGroup;
  book_id : number;
  constructor(
    private bookService : BookService,
    private router : Router
  )
  {}

  ngOnInit(): void 
  {
    
    this.createForm = new FormGroup(
      {
       
        title : new FormControl(null),
        author : new FormControl(null),
        publisher : new FormControl(null),
        image : new FormControl(null),
        pages : new FormControl(null),
        amount : new FormControl(null),
        price : new FormControl(null)
      }
    )
  }
  onCreate()
  {
    
    
    this.bookItem.title = this.createForm.value.title;
    this.bookItem.author = this.createForm.value.author;
    this.bookItem.publisher = this.createForm.value.publisher;
    this.bookItem.image = this.createForm.value.image;
    this.bookItem.numberOfPage = parseInt(this.createForm.value.pages);
    this.bookItem.amount = parseInt(this.createForm.value.amount);
    this.bookItem.price = parseFloat(this.createForm.value.price);

    console.log(this.bookItem);
    this.bookService.createNewBook(this.bookItem).subscribe(data=>
      {
        this.router.navigate(['book-management']);
        console.log('hello')
      })

}
}
