import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { Router, RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './user/user.component';

import { BookDetailComponent } from './book-detail/book-detail.component';

import { ListBooklendingComponent } from './list-booklending/list-booklending.component';
import { DatePipe } from '@angular/common';
import { ListBookItemAddedComponent } from './list-book-item-added/list-book-item-added.component';
import { AdmimPageComponent } from './admim-page/admim-page.component';
import { MemberManagementComponent } from './member-management/member-management.component';
import { BookManagementComponent } from './book-management/book-management.component';
import { ListBookInBooklendingComponent } from './list-book-in-booklending/list-book-in-booklending.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { CreateBookComponent } from './create-book/create-book.component';




@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    LoginComponent,
    HomeComponent,
    UserComponent,

    BookDetailComponent,
    ListBooklendingComponent,
    ListBookItemAddedComponent,
    AdmimPageComponent,
    MemberManagementComponent,
    BookManagementComponent,
    ListBookInBooklendingComponent,
    UpdateBookComponent,
    CreateBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule

   
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
