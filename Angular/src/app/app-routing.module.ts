import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmimPageComponent } from './admim-page/admim-page.component';
import { BookManagementComponent } from './book-management/book-management.component';
import { CreateBookComponent } from './create-book/create-book.component';
import { HomeComponent } from './home/home.component';
import { ListBookInBooklendingComponent } from './list-book-in-booklending/list-book-in-booklending.component';
import { ListBookItemAddedComponent } from './list-book-item-added/list-book-item-added.component';
import { ListBooklendingComponent } from './list-booklending/list-booklending.component';
import { LoginComponent } from './login/login.component';
import { MemberManagementComponent } from './member-management/member-management.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {path:'',redirectTo:'/home',pathMatch:'full'},
  {path:'home', component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path :'user',component:UserComponent},
  {path :'list-booklending',component:ListBooklendingComponent},
  {path : 'list-book-added',component : ListBookItemAddedComponent},
  {path :'admin',component:AdmimPageComponent},
  {path :'member-mamagement',component : MemberManagementComponent},
  {path : 'book-management', component : BookManagementComponent},
  {path : 'booklending-detail', component : ListBookInBooklendingComponent},
  {path : 'update-Book' , component : UpdateBookComponent},
  {path : 'create-Book',component:CreateBookComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[HomeComponent,LoginComponent,UserComponent];
