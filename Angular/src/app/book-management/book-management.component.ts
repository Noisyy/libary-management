import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Bookitem } from '../_module/bookitem';
import { BookService } from '../_services/book.service';

@Component({
  selector: 'app-book-management',
  templateUrl: './book-management.component.html',
  styleUrls: ['./book-management.component.css']
})
export class BookManagementComponent implements OnInit{
  listBookitem : Array<Bookitem>=[];
  constructor(
    private bookService : BookService,
    private router : Router
  ){}
  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.bookService.getAllBook()
    .subscribe(data=>{
      this.listBookitem=data;
    })
  }
  removeBook(id : number){
    this.bookService.deleteBook(id)
    .subscribe(data=>{
      alert("Delete Successfully!!");
      window.location.reload();
    })
  }
  updateBook(id : number){
    sessionStorage.setItem('bookUpdate_id',id.toString());
    this.router.navigate(['update-Book']);
  }
  createBook(){

  }
}
