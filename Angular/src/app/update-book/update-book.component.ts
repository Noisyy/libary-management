import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Bookitem } from '../_module/bookitem';
import { BookService } from '../_services/book.service';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit{

  bookItem : Bookitem = new Bookitem();
  updateForm : FormGroup;
  book_id : number;
  constructor(
    private bookService : BookService,
    private router : Router
  )
  {}

  ngOnInit(): void 
  {
    this.book_id = parseInt( sessionStorage.getItem('bookUpdate_id'));
    this.bookService.getBookItemById(this.book_id).
    subscribe(data=>{
      this.bookItem = data;
      console.log(this.bookItem);
    })
    this.updateForm = new FormGroup(
      {
        id : new FormControl(this.bookItem.id),
        title : new FormControl(this.bookItem.title),
        author : new FormControl(null),
        publisher : new FormControl(null),
        image : new FormControl(null),
        pages : new FormControl(null),
        amount : new FormControl(null),
        price : new FormControl(null)
      }
    )
  }
  onUpdate()
  {
    console.log(this.updateForm);
    this.bookItem=new Bookitem();
    this.bookItem.id = this.book_id;
    this.bookItem.title = this.updateForm.value.title;
    this.bookItem.author = this.updateForm.value.author;
    this.bookItem.publisher = this.updateForm.value.publisher;
    this.bookItem.image = this.updateForm.value.image;
    this.bookItem.numberOfPage = parseInt(this.updateForm.value.pages);
    this.bookItem.amount = parseInt(this.updateForm.value.amount);
    this.bookItem.price = parseFloat(this.updateForm.value.price);

    console.log(this.bookItem);
    this.bookService.updateBook(this.bookItem.id,this.bookItem).subscribe(data=>
      {
        this.router.navigate(['book-management']);
      })

  }
}
