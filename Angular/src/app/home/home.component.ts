import { DatePipe, JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bookitem } from '../_module/bookitem';
import { Booklending } from '../_module/booklending';
import { Member } from '../_module/member';
import { BookService } from '../_services/book.service';
import { MemberService } from '../_services/member.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  implements OnInit{
  selectedValue : string;
  listBook : Bookitem[];
  lendingBooks : Array<Bookitem>=[];
  member : Member = new Member() ;
  name : any;
  account_id : number;
  startDate: Date;
  endDate : Date;
  formatDate : string ='YYYY-MM-dd';
  bookLending : Booklending = new Booklending();
  count : number =0;
  constructor(
    private router :Router, 
    private route : ActivatedRoute, 
    private bookService : BookService,
    private memberService : MemberService,
    private datePipe : DatePipe
    
    ){}
  ngOnInit(): void {
    // this.getListBook("title");
    this.getDataOfMeber();
    
    
  }
  getDataOfMeber()
  {
    if(sessionStorage.getItem('account_id')!==null)
    {
      this.account_id = parseInt( sessionStorage.getItem('account_id'));
      this.memberService.getMemberByAccountId(this.account_id).subscribe(data=>
        {
          console.log(JSON.stringify(data));
          if(data!==null)
          {
            console.log(11);
            this.member.id = data['id'];
            this.member.name= data['name'];
            this.member.phone = data['phone'];
            this.member.address = data['address'];
            this.name = data['name'];
            sessionStorage.setItem('member_id',this.member.id.toString());
          }
        })
    }
    else{
      this.member=null;
    }
  }
  getListBook(title : string)
  {
    this.bookService.getListBookItemByTitle(title).subscribe(data=>{
      if(data!==null)
      {
        this.listBook = data;
        // console.log(JSON.stringify(this.listBook));
      }
    })
  }

  searchBook()
  {
    console.log(this.selectedValue);
    if(this.selectedValue=='title')
    {
      this.getListBook(this.selectedValue);
    }
  }
  logOut()
  {
    sessionStorage.removeItem('account_id');
    this.member=null;
    sessionStorage.clear();
    localStorage.clear();
  }
  addToBookLending(id :number)
  {
    if(this.checkLogined())
    {
      console.log(id);
    let book = new Bookitem();
    this.bookService.getBookItemById(id).subscribe(data=>
      {
        book = data;
        this.startDate = new Date();
        this.endDate = new Date();
        this.endDate.setDate(this.startDate.getDate()+4);
        let startDateFormated =this.datePipe.transform (this.startDate,this.formatDate);
        let endDateFormated =this.datePipe.transform (this.endDate,this.formatDate);

        this.bookLending.startD = startDateFormated;
        this.bookLending.endDate = endDateFormated;

        this.bookLending.list.push(book);
        this.count =this.bookLending.list.length;
        // console.log(typeof book);
        // this.lendingBooks.push(book);
        console.log(this.bookLending); 
        localStorage.setItem('tempBookLending',JSON.stringify(this.bookLending));
        sessionStorage.setItem('tempBookLending',JSON.stringify(this.bookLending));
      })
    }
    else
    {
      alert("You have to Log in , friend !!");
      
      
    }
  }
  checkLogined() : boolean
  {
    if(sessionStorage.getItem('account_id')!==null)
    {
      return true;
    }
    return false;

  }
}
