create database jpatest;
use jpatest;
create table `account`(
	`id` bigint not null primary key auto_increment,
	`username` varchar(100),
	`password` varchar(100),
	`role`	int default 2,-- 1: librariant, 2: member 
	`status` varchar(100) default 'active'
);
insert into `account`(`username`,`password`) values('cuongnguyen','cuong123');
insert into `account`(`username`,`password`) values('nhutnguyen','nhut123');
insert into `account`(`username`,`password`) values('hoanganh','nhut123');
insert into `account`(`username`,`password`,`role`) values ('admin','admin',1);
select * from account;
create table `member`(
	`id` bigint not null primary key auto_increment,
	`name` varchar(100),
	`phone`	varchar(100),
	`address` varchar(100),
	`account_id` bigint not null,
	constraint fk1_acc foreign key (`account_id`) references `account`(`id`)  ON DELETE CASCADE
);
insert into `member`(`name`,`phone`,`address`,`account_id`) values('hoang anh 1','0123456789','ha noi',1);
select * from member;
create table librariant(
	`id` bigint not null primary key auto_increment,
	`name` varchar(100),
	`phone` varchar(100),
	`account_id` bigint not null,
	constraint fk2_acc foreign key (`account_id`) references `account`(`id`)  ON DELETE CASCADE
);
insert into librariant(`name`,`phone`,`account_id`) values('librariant','0123456789',2);
create table book_lending(
	`id` bigint not null primary key auto_increment,
	`startD` date not null,
	`endD`	date not null,
	`mem_id` bigint not null,
	
    `status` varchar(100) default 'not accept',
	constraint fk1_mem foreign key (`mem_id`) references `member`(`id`)  ON DELETE CASCADE
)
;
insert into book_lending(`startD`,`endD`,`mem_id`) values('2022-12-04','2022-12-14',1);
create table book_item(
	`id`	bigint not null primary key auto_increment,
	`title`  	varchar(100) ,
	`category` varchar(100),
	`author`	varchar(100),
	`publisher`	varchar(100),
	`img` 	varchar(200),
	`numOfPage` int,
	`amount` 	int,
	`position` varchar(100),
	`price` double
   
     
);
insert into book_item (`title`,`category`,`author`,`publisher`,`img`,`numOfPage`,`amount`,`position`,`price`)
values('title1','category1','author1','publisher1','imageUrl1',234,15,'S403',200.0);
insert into book_item (`title`,`category`,`author`,`publisher`,`img`,`numOfPage`,`amount`,`position`,`price`)
values('title1','category1','author1','publisher1','imageUrl1',196,10,'S403',200.0);

create table book_lending_detail(
	`book_id` bigint not null,
    `book_lending_id` bigint not null
    
);

alter table book_lending_detail
add foreign key(`book_id`) references book_item(`id`) ;
alter table book_lending_detail
add foreign key(`book_lending_id`) references book_lending(`id`) ;
insert into book_lending_detail(`book_id`,`book_lending_id`) values(1,1);
insert into book_lending_detail(`book_id`,`book_lending_id`) values(2,1);

create table return_book(
	`id` bigint not null primary key auto_increment,
	`mem_id` bigint not null,
	`returnD` date not null,
	`book_lending_id` bigint not null,
	constraint fk1_mem_returnbook foreign key (`mem_id`) references `member`(`id`)  ON DELETE CASCADE,
	constraint fk2_bl foreign key (`book_lending_id`) references book_lending(`id`)  ON DELETE CASCADE
);

select * from book_lending_detail;