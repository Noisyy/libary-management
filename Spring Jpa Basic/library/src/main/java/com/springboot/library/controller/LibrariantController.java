package com.springboot.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.library.domain.Librariant;
import com.springboot.library.repository.LibrariantRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("librariant")
public class LibrariantController {
	
	@Autowired
	LibrariantRepository repo;
	
	@GetMapping("/{id}")
	public Librariant getByAccountId(@PathVariable Long id)
	{
		try {
			return repo.findByAccountId(id);
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}
