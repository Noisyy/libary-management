package com.springboot.library.domain;
import java.util.HashSet;
import java.util.Set;

//package com.springboot.jpa.domain;
//
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="member")
public class Member {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="name")
	private String name;
	@Column(name="phone")
	private String phone;
	@Column(name="address")
	private String address;
	
	@OneToOne( cascade=CascadeType.ALL)
	@JoinColumn(name="account_id" ,referencedColumnName = "id")
	private Account account;

	
//	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	@JsonIgnore
	@OneToMany
	@JoinColumn(name="mem_id")
	private Set<BookLending> listBookLending = new HashSet<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Member() {
		
	}

//	@JsonManagedReference
	public Set<BookLending> getListBookLending() {
		return listBookLending;
	}

	public void setListBookLending(Set<BookLending> listBookLending) {
		this.listBookLending = listBookLending;
	}

	
	
	
}
