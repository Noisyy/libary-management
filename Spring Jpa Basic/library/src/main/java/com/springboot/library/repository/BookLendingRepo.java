package com.springboot.library.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.springboot.library.domain.BookLending;

@Repository
public interface BookLendingRepo extends JpaRepository<BookLending	, Long>{

	List<BookLending> findByMemberId(Long id);

	@Query(value="select * from book_lending b where b.id=(select max(b.id) from book_lending b where b.mem_id =:id )  group by b.id;",nativeQuery = true)
	BookLending findLastestBookLendingByMemberId(Long id);

}
