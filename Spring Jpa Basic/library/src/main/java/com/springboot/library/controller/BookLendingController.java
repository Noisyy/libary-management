package com.springboot.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.library.domain.BookLending;
import com.springboot.library.domain.Member;
import com.springboot.library.repository.BookLendingRepo;
import com.springboot.library.repository.MemberRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("booklending")
public class BookLendingController {

	@Autowired
	BookLendingRepo repo;
	
	@Autowired
	MemberRepository memberRepo;

	
	@GetMapping("/list")
	public List<BookLending> findAll()
	{
		return (List<BookLending>)repo.findAll();
	}
	
	@GetMapping("/listByMemberId/{id}")
	public List<BookLending> findByMemberId(@PathVariable Long id)
	{
		return (List<BookLending>)repo.findByMemberId(id);
	}
	
	@PostMapping("/create/{id}")
	public void create(@PathVariable Long id, @RequestBody BookLending booklending)
	{
		Member m = memberRepo.findById(id).get();
		
		System.out.println(m.getName());
		System.out.println(booklending.getStatus());
		booklending.setMember(m);
		
		m.getListBookLending().add(booklending);
		repo.save(booklending);
		memberRepo.save(m);
		
//		 repo.save(booklending);
	}
	@PutMapping(value="/accept/{id}")
	public boolean acceptBooklending(@PathVariable Long id,@RequestBody String status)
	{
		//,@RequestBody String status
		try {
			BookLending lastestBooklending = repo.findLastestBookLendingByMemberId(id);
			if(lastestBooklending!=null)
			{
				lastestBooklending.setStatus(status);
				repo.save(lastestBooklending);
				return true;
			}
			return false;
		}catch (Exception e) {
			return false;
		}
	}
	
}
