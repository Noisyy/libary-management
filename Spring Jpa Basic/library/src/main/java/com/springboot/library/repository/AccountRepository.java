package com.springboot.library.repository;
//package com.springboot.jpa.repository;
//
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springboot.library.domain.Account;



@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{

	Account findByUsername(String username);

}
