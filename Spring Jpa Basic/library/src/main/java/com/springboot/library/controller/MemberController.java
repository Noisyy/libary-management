package com.springboot.library.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.library.domain.Account;
import com.springboot.library.domain.BookLending;
import com.springboot.library.domain.Member;
import com.springboot.library.repository.AccountRepository;
import com.springboot.library.repository.MemberRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("member")
public class MemberController {

	@Autowired
	MemberRepository repo;
	
	
	@Autowired
	AccountRepository accountRepo;
	
	@GetMapping("/list")
	public List<Member> getAll()
	{
		return (List<Member>) repo.findAll();
	}
	
	
	@PutMapping("/update")
	public Member update(@RequestBody Member member)
	{
		try {
			Member m = repo.findById(member.getId()).get();
			if(m!=null)
			{
				m.setAccount(member.getAccount());
				return repo.save(m);
			}
			return null;
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	@GetMapping("/findByAccountId/{id}")
	public Member findByAccountId(@PathVariable Long id)
	{

		try {
			return repo.findByAccountId(id);
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
		
		
	}
	
	@GetMapping("/{id}")
	public Member findById(@PathVariable Long id)
	{
		try
		{
			return repo.findById(id).get();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	@PostMapping("/create")
	public Member create( @RequestBody Member member)
	{
		Account a = accountRepo.findByUsername(member.getAccount().getUsername());
		if(a!=null)
		{
			System.out.println("account exist");
			return null;
		}
		member.getAccount().setRole(2);
		member.getAccount().setStatus("active");
		accountRepo.save(member.getAccount());
		a = accountRepo.findByUsername(member.getAccount().getUsername());
		member.getAccount().setId(a.getId());
		return repo.save(member);
	}
}
