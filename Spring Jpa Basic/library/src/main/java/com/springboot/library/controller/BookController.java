package com.springboot.library.controller;

import java.util.List;
import java.util.Set;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.library.domain.BookItem;
import com.springboot.library.domain.BookLending;
import com.springboot.library.repository.BookItemRepository;
import com.springboot.library.repository.BookLendingRepo;
@CrossOrigin(origins = "http://localhost:4200") 
@RestController
@RequestMapping("book")
public class BookController {
	@Autowired
	BookItemRepository repository;
	
	@Autowired
	BookLendingRepo bookLendingRepo;
	
	@GetMapping("/list")
	List<BookItem> getAllBook(){
		return (List<BookItem>) repository.findAll();
	}
	
	@GetMapping(value="/{id}")
	BookItem getBookById(@PathVariable Long id ){
		return  repository.findById(id).get();
	}
	
	@PostMapping(value="/create")
	public BookItem createBookItem(@RequestBody BookItem bookItem)
	{
		return repository.save(bookItem);
	}
	
	
	
	@GetMapping("/searchByTitle/{title}")
	public List<BookItem> findByTitle(@PathVariable String title)
	{
		return (List<BookItem>) repository.findByTitle(title);
	}
	@GetMapping("/searchByAuthor/{author}")
	public List<BookItem> findByAuthor(@PathVariable String author)
	{
		return (List<BookItem>) repository.findByAuthor(author);
	}
	@GetMapping("/searchByPublisher/{publisher}")
	public List<BookItem> findByPublisher(@PathVariable String publisher)
	{
		return (List<BookItem>) repository.findByPublisher(publisher);
	}
	
	
	@PutMapping(value="/update/{id}")
	public boolean updateBook(@PathVariable Long id, @RequestBody BookItem newBook)
	{
		
		try 
		{
			BookItem oldBook = repository.findById(id).get();
			if(oldBook!=null)
			{

				newBook.setId(id);
				repository.save(newBook);
				return true;
			}
			return false;
			
			
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	@DeleteMapping(value="/delete/{id}")
	public boolean deleteBook(@PathVariable Long id)
	{
		
		try 
		{
			BookItem book = repository.findById(id).get();
			if(book!=null)
			{

				
				repository.delete(book);
				return true;
			}
			return false;
			
			
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	@GetMapping("/findByBookLendingId/{id}")
	public Set<BookItem> findByBookLendingId(@PathVariable Long id)
	{
		BookLending b = bookLendingRepo.findById(id).get();
		return b.getList();
	}
}
