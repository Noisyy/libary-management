package com.springboot.library.controller;
//package com.springboot.jpa.controller;
//
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.library.domain.Account;
import com.springboot.library.repository.AccountRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("account")
public class AccountController {
	
	@Autowired
	AccountRepository repository;
	
	@GetMapping("/list")
	List<Account> findAllAccount(){
		return (List<Account>)repository.findAll();
	}
	
	@PostMapping(value="/login")
	public Account checkLogin(@RequestBody Account account)
	{
		Account a = repository.findByUsername(account.getUsername());
		if(a!=null)
		{
			if(a.getPassword().equals(account.getPassword()))
			{
				System.out.println("Loged in");
				return a;
			}
		}
		return null;
	}
	
	@PostMapping(value="/create")
	public Account create(@RequestBody Account account)
	{
		Account a = repository.findByUsername(account.getUsername());
		if(a!=null)
		{
			System.out.println("account exist");
			return null;
		}
		account.setRole(2);
		account.setStatus("active");
		return repository.save(account);
	}
	
	@PutMapping(value="/blockOrUnblock/{id}")
	public boolean blockOrUnblock(@PathVariable Long id, @RequestBody String status)
	{
		try
		{
			Account a = repository.findById(id).get();
			if(a!=null)
			{
				a.setStatus(status);
				repository.save(a);
				return true;
			}
			return false;
		}
		catch (Exception e) {
			return false;
		}
	}
}
