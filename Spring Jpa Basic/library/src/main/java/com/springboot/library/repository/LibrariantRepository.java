package com.springboot.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.library.domain.Librariant;


@Repository
public interface LibrariantRepository extends JpaRepository<Librariant, Long >{

	Librariant findByAccountId(Long id);

}
