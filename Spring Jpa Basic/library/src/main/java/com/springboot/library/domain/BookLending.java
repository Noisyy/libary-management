package com.springboot.library.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="book_lending")
public class BookLending {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name="startD")
	private Date startD;
	@Column(name="endD")
	private Date endDate;
	@Column(name="status")
	private String status;
	@ManyToMany
	@JoinTable(name="book_lending_detail",
				joinColumns = @JoinColumn(name="book_lending_id"),
				inverseJoinColumns = @JoinColumn(name = "book_id"))	
	private Set<BookItem> list;
	
	//, insertable = false, updatable = false
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="mem_id")
	private Member member;

	public BookLending() {
	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartD() {
		return startD;
	}

	public void setStartD(Date startD) {
		this.startD = startD;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Set<BookItem> getList() {
		return list;
	}

	public void setList(Set<BookItem> list) {
		this.list = list;
	}

	@JsonBackReference
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
