package com.springboot.library.repository;
//package com.springboot.jpa.repository;
//
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.library.domain.Member;


@Repository
public interface MemberRepository extends JpaRepository<Member, Long>{

	Member findByAccountId(Long id);

}
