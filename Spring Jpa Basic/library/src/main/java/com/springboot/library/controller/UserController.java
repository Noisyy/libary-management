//package com.springboot.library.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.springboot.library.domain.User;
//import com.springboot.library.repository.UserRepo;
//@CrossOrigin(origins = "http://localhost:4200")
//@RestController
//@RequestMapping("/user")
//public class UserController {
//
//	@Autowired
//	UserRepo repo;
//	
//	@GetMapping("/list")
//	List<User> getAll(){
//		
//		return (List<User>) repo.findAll();
//	}
//	@GetMapping("/searchById/{id}")
//	User findById(@PathVariable Long id)
//	{
//		try {
//			User u =   repo.findById(id).get();
//			return u;
//		}catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
//	}
//	@PostMapping(value="/create")
//	public User addUser(@RequestBody User u)
//	{
//		try {
//		System.out.println(u.getName());
//		return repo.save(u);
//		}catch (Exception e) {
//			// TODO: handle exception
//			return null;
//		}
//	}
//	
//	@PostMapping("/login")
//	public User checkLogin(@RequestBody User u)
//	{
//		try {
//			User nUser = repo.findByUsername(u.getName());
//			if(nUser!=null) {
//				if(u.getPassword().equals(nUser.getPassword())) 
//				{
//					System.out.println("Checked");
//					return nUser;
//				}
//			}
//			
//			return null;
//		}catch(EmptyResultDataAccessException e)
//		{
//			return null;
//		}
//		
//		
//	}
//	
//	@GetMapping("/name/{name}")
//	User findByUserName(@PathVariable("name") String name)
//	{
//		try {
//			return   repo.findByName(name);
//			
//		}catch (Exception e) {
//			// TODO: handle exception
//			
//			return new User(1L,"Wrong Result","Wrong Result");
//		}
//	}
//}
