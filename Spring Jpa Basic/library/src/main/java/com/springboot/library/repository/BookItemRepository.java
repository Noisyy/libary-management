package com.springboot.library.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.library.domain.BookItem;

@Repository
public interface BookItemRepository extends JpaRepository<BookItem	, Long>{

//	List<BookItem> findByName(String name);

	@Query(value="select * from book_item b where b.title like %:title% ",nativeQuery = true)
	List<BookItem> findByTitle(@Param("title") String title);

	@Query(value="select * from book_item b where b.author like %:author% ",nativeQuery = true)
	List<BookItem> findByAuthor(String author);

	@Query(value="select * from book_item b where b.publisher like %:publisher% ",nativeQuery = true)
	List<BookItem> findByPublisher(String publisher);


}
